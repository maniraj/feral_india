#include <SD.h>

const int chipSelect = 4;

void setup()
{  
  Serial.begin(9600);
  Serial.print("Initializing SD card...");
  pinMode(4, OUTPUT);

  if (!SD.begin(chipSelect))
  {
    Serial.println("Card failed, or not present");
     return;
  }
  Serial.println("Card initialized.");
}

void loop()
{
  String dataString = "Hello!";

File dataFile = SD.open("datalog.txt", FILE_WRITE);

  if (dataFile)
  {
    dataFile.println(dataString);
    dataFile.close();
    Serial.println(dataString);
  } 
  else
  {

    Serial.println("ERROR!, Opening datalog.txt File");

  }
}
