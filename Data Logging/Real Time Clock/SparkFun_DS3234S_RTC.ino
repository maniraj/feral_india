#include <SPI.h>
#include <SparkFunDS3234RTC.h>

#define CHIP_PIN 10

void setup() {
  Serial.begin(9600);

  rtc.begin(CHIP_PIN);
  rtc.autoTime();
  rtc.update();
}

void loop() 
{
  static int8_t lastSecond = -1;
  rtc.update();

  if (rtc.second() != lastSecond)
  {
    printTime();
    
    lastSecond = rtc.second();
  }
}

void printTime()
{
  Serial.print(String(rtc.hour()) + ":");
  Serial.print(String(rtc.minute()) + ":");
  Serial.print(String(rtc.second()) + ":");
  
  Serial.print(String(rtc.date()) + ",");
  Serial.print(String(rtc.month()) + ",");
  Serial.println(String(rtc.year()));
}
