# Reason for Repository:
Created for Documents sharing, codes, designs and other technical & non-technical progress etc..,

# Objectives:
Implementing the sensors and data logging for further process.
    
# Work Plan:

- Transducer Selection.
- Data Acquisition & Logging.
    - Micro-controller.
    - Data transfer systems for data/diagnostics:
    - Networking & Telemetry (GSM/BtLE/LoRA) or (LAN/USB).
- Soil moisture monitoring.
- Air quality monitoring.
- Logging module for tipping bucket rain gauges.
- Water quality monitoring.
- Fixing FREE STATION
