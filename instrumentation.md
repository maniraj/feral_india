# Block Diagram:
![Block Diagram](Soil Moisture Sensor/Water Mark/images/Instrumentation_Block_Diagram_general.png)

# Introduction:
Soil moisture sensors measure the volumetric water content in soil. Soil moisture sensors measure the volumetric water content indirectly by using some other property of the soil, such as electrical resistance, dielectric constant, or interaction with neutrons, as a proxy for the moisture content. The relation between the measured property and soil moisture must be calibrated and may vary depending on environmental factors such as soil type, temperature, or electric conductivity. Soil moisture sensors typically refer to sensors that estimate volumetric water content. Another class of sensors measure another property of moisture in soils called water potential; these sensors are usually referred to as soil water potential sensors and include tensiometers and gypsum blocks. The target of connecting the moisture sensor is to be able to measure the resistance presented by the sensor.

## Watermark sensor:
(Watermark 200ss moisture sensor)

The Watermark 200SS sensor is a solid-state electrical resistance sensing device that is used to measure soil water tension. As the tension changes with water content the resistance changes as well. The sensor consists of a pair of highly corrosion resistant electrodes that are imbedded within a granular matrix. A current is applied to the
sensor to obtain a resistance value.

### How does it work?

The sensor responds to changes in soil moisture. Once planted in the soil, it exchanges
water with the surrounding soil thus staying in equilibrium with it.

Soil water is an electrical conductor thereby providing a relative indication of the soil moisture status. As the soil dries, water is removed from the sensor and the resistance measurement increases. Conversely, when the soil is rewetted, the resistance lowers.

This sensor is unique in that it takes its resistive measurement within a defined and consistent internal matrix material, rather than using the surrounding soil as the measurement medium. This unique feature allows the sensor to have a stable and consistent calibration that does not need to be established for every installation.

The relationship of ohm of resistance to centibars (cb) or kilopascals (kPa) of soil water tension is constant and built into the reading devices that are used to interrogate the sensor. The sensor is calibrated to report soil water tension, or matric potential, which is the best reference of how readily available soil water is to a plant.

### Technical specifications:

- Range of measurement from 0 to 239 cb (kPa)
- Stainless steel enclosure
- Fully solid-state
- Will not dissolve in soil
- Not affected by freezing temperatures
- Internally compensated for commonly found salinity levels
- Easy to use
- No maintenance required
- Dimensions:
       - Diameter: 22 mm
       - Lenght: 83 mm
       - Weight: 0.067 kg
       - Wire length: 1.5 m
- [Datasheet](Soil Moisture Sensor/Water Mark/datasheet/watermark-soil-moisture-sensor-model-200ss.pdf)

This sensor is compatible with AC or DC reading devices, but a specialized circuit is required.
Outputs values just like the 200SS-VA, as a 0-2.8 volt linear reference 
of soil water tension from 0 to 239 centibars (kPa)

### Installation:

(Watermark 200SS sensors must be set following these steps):
- Depth: It depends on the depth of the crop roots that depend, at the same time, on the soil depth and texture. To set the sensors in the effective root zone is the adequate criteria.
- Superficial rooting crops: (less than 35 cm) one sensor is enough
- Deep rooting crops: (grains, vine, trees) soil moisture must be measured at least in two depths

(Watermark 200ss sensor installation)
- Wet the sensors with irrigation water during night-time. Set the sensors always wet
- Using an iron tube of 7/8’’, make a hole in the ground to the desired depth. In very dense or thick texture soils, the hole must be a bit wider (25-30mm) and it must be refilled afterwards with mud porridge. In order to ensure the proper working of the sensor is mandatory to make it fit perfectly to the hole. The ideal way of making the hole is by using an installation tool. This must make the hole with a except for the lower end, where the hole must have the exact same diameter as the sensor.
- Fill the hole with water and introduce the sensor until it reaches the bottom. In order to perform this action, it can be pushed with a piece of a 20mm PVC tube.
-  Refill the hole with mud porridge in order to erase any air bag.
-  If you wish so, the PVC tube can be set in the hole by sticking it to the sensor. The wires can pass through the tube and fixed to the upper part.

### Code:
Arduino code [Here](Soil Moisture Sensor/Water Mark/code/watermark_sensor.ino)

# Temperature sensor:
(Soil temperature sensor DS18B20)

This is a pre-wired and waterproofed version of the DS18B20 sensor. Handy for when you need to measure something far away, or in wet conditions. While the sensor is good up to 125°C the cable is jacketed in PVC so we suggest keeping it under 100°C. Because they are digital, you don’t get any signal degradation even over long distances! These 1-wire digital temperature sensors are fairly precise (±0.5°C over much of the range) and can give up to 12 bits of precision from the onboard digital-to-analog converter. They work great with any microcontroller using a single digital pin, and you can even connect multiple ones to the same pin, each one has a unique 64-bit ID burned in at the factory to differentiate them. Usable with 3.0-5.0V systems.

### Technical Specifications:

- Stainless steel tube 6mm diameter by 30mm long
- Cable is 36″ long / 91cm, 4mm diameter
- Usable temperature range: 55 to 125 ºC
- Power supply: 3 to 5.5V
- [Datasheet](Temperature Sensor/datasheet/DS18B20.pdf)
    
# SD Card module:

The module (MicroSD Card Adapter) is a Micro SD card reader module, and the SPI interface via the file system driver, microcontroller system to complete the MicroSD card read and write files. This Arduino SD Card Shield is a simple solution for transferring data to and from a standard SD card. The pinout is directly compatible with Arduino, but can also be used with other microcontrollers. Through programming, you can read and write to the SD card using your arduino

### Technical specifications:
- Power supply: 4.5V – 5.5V, 3.3V voltage regulator circuit board
- Positioning holes: 4 M2 screws positioning hole diameter of 2.2mm
- Control Interface: GND, VCC, MISO, MOSI, SCK, CS
- Size: 45 x 28mm
- Net weight: 6g

# Micro-Controller:
(Arduino UNO R3)

The Arduino Uno R3 is a microcontroller board based on the ATmega328. It has 14 digital input/output pins (of which 6 can be used as PWM outputs), 6 analog inputs, a 16 MHz crystal oscillator, a USB connection, a power jack, an ICSP header, and a reset button. It contains everything needed to support the microcontroller; simply connect it to a computer with a USB cable or power it with a AC-to-DC adapter or battery to get started.

### Technical specifications:
- Microcontroller 	ATmega328
- Operating Voltage 	5V
- Input Voltage (recommended) 	7-12V
- Input Voltage (limits) 	6-20V
- Digital I/O Pins 	14 (of which 6 provide PWM output)
- Analog Input Pins 	6
- DC Current per I/O Pin 	40 mA
- DC Current for 3.3V Pin 	50 mA
- Flash Memory 	32 KB (ATmega328) of which 0.5 KB used by bootloader
- SRAM 	2 KB (ATmega328)
- EEPROM 	1 KB (ATmega328)
- Clock Speed 	16 MHz
- [Datasheet](Data Logging/Microcontroller/datasheet/Arduino_Uno_Rev3-schematic.pdf)
