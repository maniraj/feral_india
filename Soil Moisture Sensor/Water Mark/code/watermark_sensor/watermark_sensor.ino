#include <SPI.h>
#include <SD.h>

const int chipSelect = 53;

//Analog Temperature Measurement
float tempC;       //Temeperature in Celcius
int reading;       //A measurand variable for temperature
int tempPin = 1;   //Analog input Pin to read the Temperature

//Analog Soil Moisture Resistance Measurement
int pinHumVp = 13; //Tension divider fixed resistance pin
int pinHumVn = 12; //Tension divider sensor pin
int pinHumA = 0;   //Analog input connected to the midpoint of the voltage divider

long Rf = 22000;    //Fixed resistance of the voltage divider
float R = 0;

float Vddio = 5 ; //Operating voltage of the arduino

void setup() {
  // put your setup code here, to run once:
    Serial.begin(9600);

    pinMode(A0, INPUT); // for analog soil moisture, resistance measurement
    pinMode(A1, INPUT); // for analog temperature measurement
    pinMode(pinHumVp, OUTPUT);
    pinMode(pinHumVn, OUTPUT);
    
//SD card
    Serial.print("Initialing SD card!");
    pinMode(53, OUTPUT);

    if (!SD.begin(chipSelect))
    {
      Serial.print("Card failed, or not present");  
      return;
    }
    Serial.print("Card initialized.");

}

void loop() {
  // put your main code here, to run repeatedly:
  Serial.println("----------------------------");
  Serial.print("Soil temperature: ");
  readTemp();
  Serial.print("Sensor resistance: ");
  readRes();
  Serial.print("Soil moisture: ");
  getCentBar();
  Serial.println("~~~~~~~~~~~~~~~~~~~~~~~~~~~~");
  Serial.println(" ");

//SD card
File dataFile = SD.open("datalog.txt", FILE_WRITE);

    if (dataFile)
    {
      dataFile.print(readTemp());
      dataFile.print(",");
      dataFile.print(readRes());
      dataFile.print(",");
      dataFile.println(getCentBar());
      dataFile.close();
    } 
    else
    {
      Serial.println("ERROR! Opening datalog.txt");
    }
  }

float readTemp()
{ //refer: https://playground.arduino.cc/Main/LM35HigherResolution

  reading = analogRead(tempPin);
  tempC = 35; //reading / 9.31;
  Serial.println(tempC);
  delay(500);
  return tempC;
}

float readRes()
{
    int i = 0;
    float V,Rs,Rst = 0;
    int val;

  i = 0;
  while (i < 10)
  {
      if (i % 2 == 0)
      {
            //Mode 1 sensor to ground
            digitalWrite(pinHumVp, HIGH);
            digitalWrite(pinHumVn, LOW);
            
            delay(50);
            
            //Serial.print("Measured Tension (sensor to GND): ");
            val = analogRead(pinHumA);
            //Serial.println(val);
            V = val*Vddio/1023;
            
            Rs = (V*Rf/(Vddio-V)); //Ohms
              /*Serial.print("Rs con ");
              Serial.print(Vddio);
              Serial.print("V pin ");
              Serial.print(pinHumVp);
              Serial.print(": \t");
              Serial.println(Rs);
              Serial.print("Measured Tension: \t");
              Serial.println(V);*/
      }
      else
      {
          //2 sensor mode to Vddio
          digitalWrite(pinHumVp, LOW);
          digitalWrite(pinHumVn, HIGH);
  
          delay(50);
         
          //Serial.print("Measured Tension (sensor to Vddio): ");
          val = analogRead(pinHumA);
          //Serial.println(val);
          V = val*5.0/1023;

          Rs = (Rf*(Vddio-V)/V); //Ohms
            /*Serial.print("Rs con ");
            Serial.print(Vddio);
            Serial.print("V pin ");
            Serial.print(pinHumVn);
            Serial.print(": \t");inf
            Serial.println(Rs);
            Serial.print("Measured Tension: \t");
            Serial.println(V);*/
      }
      i++;
      Rst+=Rs;
   }
    //We disconnect the power to the sensor
    digitalWrite(pinHumVp, LOW);
    digitalWrite(pinHumVn, LOW);
    R=Rst/i;
    Serial.println(R);
    return R;
}

float getCentBar()
{
  float temp,cB;
    temp = tempC;
    cB=(4.093+(3.213*R/1000))/(1-(0.009733*R/1000)-(0.01205*temp));
    Serial.println(cB);
    return(cB);
}
