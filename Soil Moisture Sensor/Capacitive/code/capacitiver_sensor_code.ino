#include <SPI.h>
#include <SparkFunDS3234RTC.h>
#include <SD.h>


#define CHIP_PIN 10

void setup() {
  Serial.begin(9600);

  rtc.begin(CHIP_PIN);
  rtc.autoTime();
  rtc.update();

//SD card  
  Serial.print("Initializing SD card...");
  pinMode(4, OUTPUT);

  if (!SD.begin(chipSelect))
  {
    Serial.println("Card failed, or not present");
     return;
  }
  Serial.println("Card initialized.");
}

void loop()
{
	Serial.println("----------------");

  static int8_t lastSecond = -1;
  rtc.update();

  if (rtc.second() != lastSecond)
  {
    printTime();
    
    lastSecond = rtc.second();
  }


//SD card
File dataFile = SD.open("datalog.txt", FILE_WRITE);

  if (dataFile)
  {
    dataFile.println(dataString);
    dataFile.close();
    Serial.println(dataString);
  } 
  else
  {

    Serial.println("ERROR!, Opening datalog.txt File");

  }
}

void printTime()
{
  Serial.print(String(rtc.hour()) + ":");
  Serial.print(String(rtc.minute()) + ":");
  Serial.print(String(rtc.second()) + ":");
  
  Serial.print(String(rtc.date()) + ",");
  Serial.print(String(rtc.month()) + ",");
  Serial.println(String(rtc.year()));
}
